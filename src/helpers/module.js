import { ScreenEnum } from "@/enums/screen_enum";

export class Module {
    static setModuleLoad(screen) {
        switch (screen) {
            case ScreenEnum.Initialize:
                return "initializemodule/loadData";
            case ScreenEnum.Init:
                return "intromodule/loadData";
            case ScreenEnum.Language:
                return "langmodule/loadData";
            case ScreenEnum.Unit:
                return "processmodule/loadData";
            case ScreenEnum.DebtInquiry:
                return "debtinquirymodule/loadData";
            case ScreenEnum.DebtSelection:
                return "debtselectionmodule/loadData";
            case ScreenEnum.CashType:
                return "cashtypemodule/loadData";
            case ScreenEnum.CashAccepting:
                return "cashacceptingmodule/loadData";
            case ScreenEnum.CreditCard:
                return "creditcardmodule/loadData";
            case ScreenEnum.CardReader:
                return "cardreadermodule/loadData";
            case ScreenEnum.CalculateCredit:
                return "calculatecreditmodule/loadData";
            case ScreenEnum.Print:
                return "printmodule/loadData";
            case ScreenEnum.Payback:
                return "paybackmodule/loadData";
            case ScreenEnum.EksLoad:
                return "eksloadmodule/loadData";
            case ScreenEnum.OperatorLogin:
                return "loginmodule/loadData";
            case ScreenEnum.OperatorMenu:
                return "opmenumodule/loadData";
            case ScreenEnum.OperatorCashDispense:
                return "oprecyclermodule/loadData";
            case ScreenEnum.OperatorCoin:
                return "silomodule/loadData";
            case ScreenEnum.OperatorKaset:
                return "kasetmodule/loadData";
            case ScreenEnum.OperatorReport:
                return "eodmodule/loadData";
            case ScreenEnum.OperatorMakbuz:
                return "opmakbuzmodule/loadData";
            case ScreenEnum.OperatorAlarm:
                return "opalarmmodule/loadData";
            default:
                return "";
        }
    }

    static setModuleChange(screen) {
        switch (screen) {
            case ScreenEnum.Initialize:
                return "initializemodule/changeData";
            case ScreenEnum.Init:
                return "intromodule/changeData";
            case ScreenEnum.Language:
                return "langmodule/changeData";
            case ScreenEnum.Unit:
                return "processmodule/changeData";
            case ScreenEnum.DebtInquiry:
                return "debtinquirymodule/changeData";
            case ScreenEnum.DebtSelection:
                return "debtselectionmodule/changeData";
            case ScreenEnum.CashType:
                return "cashtypemodule/changeData";
            case ScreenEnum.CashAccepting:
                return "cashacceptingmodule/changeData";
            case ScreenEnum.CreditCard:
                return "creditcardmodule/changeData";
            case ScreenEnum.CardReader:
                return "cardreadermodule/changeData";
            case ScreenEnum.CalculateCredit:
                return "calculatecreditmodule/changeData";
            case ScreenEnum.Print:
                return "printmodule/changeData";
            case ScreenEnum.Payback:
                return "paybackmodule/changeData";
            case ScreenEnum.EksLoad:
                return "eksloadmodule/changeData";
            case ScreenEnum.OperatorLogin:
                return "loginmodule/changeData";
            case ScreenEnum.OperatorMenu:
                return "opmenumodule/changeData";
            case ScreenEnum.OperatorCashDispense:
                return "oprecyclermodule/changeData";
            case ScreenEnum.OperatorCoin:
                return "silomodule/changeData";
            case ScreenEnum.OperatorKaset:
                return "kasetmodule/changeData";
            case ScreenEnum.OperatorReport:
                return "eodmodule/changeData";
            case ScreenEnum.OperatorMakbuz:
                return "opmakbuzmodule/changeData";
            case ScreenEnum.OperatorAlarm:
                return "opalarmmodule/changeData";
            default:
                return "";
        }
    }
}