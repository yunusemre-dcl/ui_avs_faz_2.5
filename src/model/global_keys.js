import { ScreenEnum } from "@/enums/screen_enum";
import { RoutePaths } from "@/router/route-paths";

export class GlobalKeys {
    static clientKey = "";
    static current_path = RoutePaths.Home;
    static current_screen = ScreenEnum.Init;
    static current_dispatch = null

    static setClientKey(key) {
        this.clientKey = key;
    }

    static setCurrentScreen(screen) {
        console.log("current screen : ", screen)
        this.current_screen = screen;
    }

    static setCurrentPath(path){
        this.current_path = path;
    }

    static setDispatch(dispatch) {
        this.current_dispatch = dispatch
    }
}