class Message {
    msgTypeId
    message
    timeout
    actionType

    constructor(msgTypeId, message, timeout, actionType) {
        this.msgTypeId = msgTypeId
        this.message = message
        this.timeout = timeout
        this.actionType = actionType
    }
}