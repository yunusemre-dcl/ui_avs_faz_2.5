export class ScrPacket {
    header;
    body;
}

export class Header {
    messageId;
    messageFlow;
    client;
    method;
}

export class Body {
    screen;
    cmd;
    data;
}