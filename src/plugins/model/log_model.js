export class LogModel {
    priorityId
    levelId
    message

    constructor(pId, lvlId, msg) {
        this.priorityId = pId
        this.levelId = lvlId
        this.message = msg
    }
}