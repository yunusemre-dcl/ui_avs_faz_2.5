import { PrintMutations } from "@/enums/mutations";
import { ScreenEnum } from '@/enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports';
import Vue from "vue";
import { ScreenCommands } from "../../enums/command";

const state = {
    title: "",
    message: "Makbuzunuzu almayı unutmayın",
    timeout: 10000,
}

const mutations = {
    [PrintMutations.SET_DATA](state, payload) {
        console.log("print setData : ", payload)
        state.message = payload.printBuffer
        state.timeout = payload.timeout
        console.log("message : ", state.message)
        console.log("timeout : ", state.timeout)
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[PRINT] SET DATA MUTATION`, { payload: payload, state: state })
    },
}

const actions = {
    // eslint-disable-next-line no-unused-vars
    loadData({ commit }, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[PRINT] LOAD DATA ACTION`, payload)
    },
    timeout() {
        var body = {};
        body.cmd = ScreenCommands.PrintTimeout;
        body.screen = ScreenEnum.Print;
        body.data = "";
        console.log("Print timeout ", body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[PRINT] TIMEOUT`, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}