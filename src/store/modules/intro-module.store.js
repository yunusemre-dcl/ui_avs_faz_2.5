import { MessageModuleActions } from "@/constants/message-constants";
import { ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import Vue from "vue";

const state = {}

const mutations = {}

const actions = {
    // eslint-disable-next-line no-unused-vars
    loadData({commit}, payload){
    },
    nextScreen({ dispatch }) {
        dispatch(MessageModuleActions.ShowLoading, null, { root:true })

        var body = {};
        body.cmd = ScreenCommands.InitStartTransaction;
        body.screen = ScreenEnum.Init;
        body.data = "";
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}