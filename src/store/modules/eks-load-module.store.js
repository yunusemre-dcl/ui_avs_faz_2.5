

import { EksLoadMutations } from '@/constants/eks-load-constants';
import { ScreenCommands } from '@/enums/command';
import { ScreenEnum } from '@/enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports';
import Vue from 'vue';

const state = {
    componentKey: 0,
    infoMessage: "",
}
const mutations = {
    [EksLoadMutations.SET_DATA](state, payload) {
        state.infoMessage = payload.infoMessage
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[EKS LOAD] Set Data Mutation`, { payload: payload, state: state })
    },
    [EksLoadMutations.CHANGE_DATA](state, payload) {
        state.infoMessage = payload.infoMessage
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[EKS LOAD] Change Data Mutation`, { payload: payload, state: state })
    },
}
const actions = {
    loadData({ commit }, payload) {
        console.log("EKS Load Data : ", payload)
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[EKS LOAD] Load Data`, payload)
        if (payload != null) {
            commit(EksLoadMutations.SET_DATA, payload.data)
        }

        var body = {};
        body.cmd = ScreenCommands.EksLoadOK;
        body.screen = ScreenEnum.EksLoad;
        body.data = "";
        console.log("Eks Load OK invoke: ", body);
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}