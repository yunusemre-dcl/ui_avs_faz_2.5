import { showEmptyDebtList, showLoading } from "@/constants/message-constants";
import { CommonCommand, DebtSelectionCommand, ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports';
import Vue from "vue";
import { DebtSelectionMutations } from "../../constants/debt-selection-constants";

const defaultState = {
    title: "Vadesi geçen borçlarınızı tarih sırasına göre ödeyebilirsiniz",
    totalPayment: 0,
    totalBillCount: 0,
    selectedPayment: 0,
    selectedBillCount: 0,
    billList: [],
    customer: {
        displayName: "",
        qTypeName: "",
        queryNo: "",  
    },
    nextLabel: "İleri",
    prevLabel: "Geri",
    cancelLabel: "İptal",
    lblTotalAmount: "Toplam Tutar",
    lblSelectedBillCount: "Seçili Borç Sayısı",
    lblTotalPayment: "",

    clmTotal: "Toplam",
    clmLastPaymentDate: "Son Ödeme Tarihi",
    clmInterest: "Gecikme",
    clmDescription: "Türü",
    clmAmount: "Tutar",

    hideBackButton: false,
    selectedAllGroupBills: false,
    nextButtonDisable: true,
}

const state = defaultState

const mutations = {
    [DebtSelectionMutations.SET_DEBTS](state, payload) {
        state.totalPayment = payload.billTotal;
        state.totalBillCount = payload.billCount;
        state.hideBackButton = false
        state.billList = payload.bills.map(e => {
            e.selected = false
            return e
        });
        state.customer = payload.cust;
    },
    [DebtSelectionMutations.SET_SELECTED_BILL](state, payload) {
        var groupBills = state.billList.filter(e => e.groupNo === payload.groupNo)
        groupBills.forEach(bill => {
            bill.selected = !bill.selected
            if (bill.selected) {
                state.selectedBillCount++
                state.selectedPayment += bill.billTotal
            }
            else {
                state.selectedBillCount--
                state.selectedPayment -= bill.billTotal
            }

        })

        state.nextButtonDisable = state.selectedBillCount == 0
    },
    [DebtSelectionMutations.RESET](state) {
        state.totalPayment = 0;
        state.totalBillCount = 0;
        state.selectedPayment = 0;
        state.selectedBillCount = 0;
    }
}

const actions = {
    loadData({ state, commit }, payload) {
        if (payload != null) {
            commit(DebtSelectionMutations.RESET)
            commit(DebtSelectionMutations.SET_DEBTS, payload.data)
            if(state.billList.length == 0) {
                var message = `${state.customer.queryNo} numaralı abone borcu bulunamadı`
                showEmptyDebtList(message)
            }
        }
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.DebtSelection;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT LIST] BACK`, body)
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    next({ state, commit }) {
        showLoading()
        var debtListData = {}
        debtListData.selectedBills = state.billList.filter(e => e.selected).map(e => e.billNo).join(",")

        var body = {};
        body.cmd = ScreenCommands.DebtSelectionSelect;
        body.screen = ScreenEnum.DebtSelection;
        body.data = JSON.stringify(debtListData);
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT LIST] NEXT`, body)
        Vue.prototype.$invokeMethod(body);
    },
    itemSelected({ commit }, payload) {
        commit(DebtSelectionMutations.SET_SELECTED_BILL, payload)
    },
    // eslint-disable-next-line no-unused-vars
    allItemSelected({ commit, state, dispatch }, payload) {
        var inChangedDebtSelData = {
            procId: payload.proc.procId,
            billGroupId: state.selectedGroup.billGroupId,
            billId: 0,
            flagCheckAll: !state.selectedAllGroupBills,
        };

        var body = {};
        body.cmd = DebtSelectionCommand.Selected;
        body.screen = ScreenEnum.DebtSelection;
        body.data = JSON.stringify(inChangedDebtSelData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.DebtSelection;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT LIST] CANCEL`, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}