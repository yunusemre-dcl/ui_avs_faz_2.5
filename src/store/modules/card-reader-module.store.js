import { MessageContents, showLoading } from '@/constants/message-constants'
import { ScreenCommands } from "@/enums/command"
import { CardReaderMutations } from "@/enums/mutations"
import { ScreenEnum } from '@/enums/screen_enum'
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports'
import Vue from "vue"

const moduleName = "cardreadermodule";

export const CardReaderActionTypes = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

const state = {
    componentKey: 0,
    title: "",
    flagShowAnimation: false,
    infoMessage: "",
    animationCode: 0,
    arrowDirection: 0,
    cancelLabel: "İptal",
    clickableCancel: false,
    headerVisible: false,
    hideBackButton: false,
    hideCancelButton: false,
}
const mutations = {
    [CardReaderMutations.SET_DATA](state, payload) {
        state.clickableCancel = true
        state.hideBackButton = true
        state.hideCancelButton = false
        state.flagShowAnimation = false
        state.animationCode = payload.animationCode
        state.title = payload.header
        state.infoMessage = payload.infoMessage
        state.arrowDirection = payload.arrowDirection
        state.headerVisible = payload.headerVisible
        state.componentKey++
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] Set Data Mutation`, { payload: payload, state: state })
        console.log("data aktarımı tamamlandı : ", state)
    }
}
const actions = {
    loadData({ commit }, payload) {
        console.log("Card Reader Load Data : ", payload)
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] Load Data`, payload)
        if (payload != null) {
            commit(CardReaderMutations.SET_DATA, payload.data)
        }
    },
    changeData() {
        showLoading(MessageContents.LOADING_PROCCESS)

        var body = {};
        body.cmd = ScreenCommands.CardReaderChangeOK;
        body.screen = ScreenEnum.CardReader;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] CHANGEDATA İŞLENDİ`, body)
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.CardReader;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CARD READER] CANCEL İŞLENDİ`, body)
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}