
import { CalculateCreditMutations } from '@/constants/calculate-credit-constants';
import { showLoading, showWarning } from "@/constants/message-constants";
import { ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports';
import Vue from "vue";

const state = {
    componentKey: 0, 
    title: "",
    subscriber: null,
    enteredAmount: null,
    showKeyboard: false,
    showNumberpad: false,
    lblAmount: "Yüklenecek tutarı giriniz",
    minAmount: 0.0,
    maxAmount: 0.0,
    nextLabel: "İleri",
    prevLabel: "Geri",
    cancelLabel: "İptal",
    infoMessage: "",
}

const mutations = {
    [CalculateCreditMutations.SET_DATA](state, payload) {
        //LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CALCULATE CREDIT] Set Data Mutation Started`)
        console.log("setData payload : ", payload)
        state.enteredAmount = null
        state.showNumberpad = true
        state.showKeyboard = false
        state.subscriber = payload.cust
        state.infoMessage = payload.infoMessage
        state.minAmount = payload.minAmount
        state.maxAmount = payload.maxAmount
        state.title = `En az ${state.minAmount} - En fazla ${state.maxAmount} kredi yüklemesi yapabilirsiniz`

        console.log("setData state : ", state)

        state.componentKey++
        //LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CALCULATE CREDIT] Set Data Mutation`, { payload: payload, state: state })
    },
    [CalculateCreditMutations.CHANGE_AMOUNT](state, payload) {
        if (state.enteredAmount == null) state.enteredAmount = ""
        if (payload == null) {
            state.enteredAmount = state.enteredAmount.slice(0, -1)
        } else
            state.enteredAmount += payload.toString();

        console.log(state.enteredAmount)
    },
}

const actions = {
    loadData({ commit }, payload) {
        console.log("calculateCreditModule loadData")
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CALCULATE CREDIT] Load Data Action`, payload)
        if (payload != null) {
            commit(CalculateCreditMutations.SET_DATA, payload.data)
        }
    },
    changeAmount({ commit }, payload) {
        commit(CalculateCreditMutations.CHANGE_AMOUNT, payload)
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.CalculateCredit;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    next({ state }) {

        if(state.enteredAmount<state.minAmount || state.enteredAmount > state.maxAmount) {
            showWarning(state.title)
            return
        }

        showLoading()

        var amount = state.enteredAmount

        if(amount == null) amount = 0;

        var model = {};
        model.amount = amount

        console.log(model);
        var body = {};
        body.cmd = ScreenCommands.CalculateCreditSend;
        body.screen = ScreenEnum.CalculateCredit;
        body.data = JSON.stringify(model);

        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L1, `[CALCULATE CREDIT] Next Action : `, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}