
import { MessageActionTypeEnum, MessageCode, MessageContents, MessageModuleActions, MessageMutations, MessageResponseEnum, showAddTime } from "@/constants/message-constants";
import { ScreenCommands } from '@/enums/command';
import { uniqueId } from "@/helpers/helper";
import { GlobalKeys } from '@/model/global_keys';
import router from '@/router';
import { RoutePaths } from '@/router/route-paths';
import Vue from 'vue';

const state = {
    componentKey: 0,
    messages: [],
    yesLabel: "Evet",
    noLabel: "Hayır",
    okLabel: "Tamam",
    timerList: [],
    timerId: -1,
    timerState: null,
    timeoutCount: 0,
}

const mutations = {
    [MessageMutations.SET_TIMER_STATE](state, payload) {
        state.timerState = payload
        console.log("Set Timer State : ", state)
    },
    [MessageMutations.ADD_MESSAGE](state, payload) {
        let message = {
            ...payload.message
        };
        message.msgId = uniqueId()
        state.messages = [...state.messages, message]
    },
    [MessageMutations.DELETE_MESSAGE](state, payload) {
        state.messages = state.messages.filter(item => item.msgId !== payload.msgId);
    },
    [MessageMutations.CLEAR_MESSAGES](state) {
        state.messages = [];
        state.componentKey++
    },
    [MessageMutations.SET_TIMER](state, payload) {
        console.log(`"timerId compare : ${state.timerId} - ${payload}`)
        if (state.timerId !== payload) clearTimeout(state.timerId)
        state.timerId = payload
    },
    [MessageMutations.SET_TIMEOUT_COUNT](state, payload) {
        state.timeoutCount = payload
    },
    [MessageMutations.INCREASE_TIMEOUT_COUNT](state) {
        state.timeoutCount++
    }
}

const actions = {
    loadTimerState({ commit }, payload) {
        commit(MessageMutations.SET_TIMER_STATE, payload)
    },
    // eslint-disable-next-line no-unused-vars
    responseMessage({ state, commit, dispatch }, payload) {
        switch (payload.msgCode) {
            case MessageCode.TIMEOUT:
                if (payload.responseState == MessageResponseEnum.Yes) {
                    commit(MessageMutations.CLEAR_MESSAGES)
                    dispatch(MessageModuleActions.StartTimeout, payload, { root: true })
                } else {
                    commit(MessageMutations.CLEAR_MESSAGES)
                    if (state.timerState.cancel != undefined) {
                        dispatch(MessageModuleActions.ShowLoading, MessageContents.LOADING_CANCEL, { root: true })
                        state.timerState.cancel()
                    } else if (state.timerState.defaultTimeout != undefined && state.timerState.defaultTimeout) {
                        dispatch(MessageModuleActions.ShowLoading, MessageContents.LOADING_CANCEL, { root: true })
                        dispatch(MessageModuleActions.CancelTimer, null, { root: true })
                    }
                    else {
                        if (GlobalKeys.current_path != RoutePaths.Introduction) router.replace({ name: RoutePaths.Introduction });
                    }
                }
                break;
            case MessageCode.CANCEL:
                if (payload.responseState == MessageResponseEnum.Yes) {
                    console.log("ResponseMessage Cancel Yes, Nezih : ", state.timerState)
                    commit(MessageMutations.CLEAR_MESSAGES)
                    dispatch(MessageModuleActions.ShowLoading, MessageContents.LOADING_CANCEL, { root: true })
                    if (state.timerState.cancel != undefined) state.timerState.cancel()
                    else dispatch(MessageModuleActions.CancelTimer, null, { root: true });
                } else if (payload.responseState == MessageResponseEnum.No) {
                    commit(MessageMutations.CLEAR_MESSAGES)
                    dispatch(MessageModuleActions.StartTimeout, state.timerState, { root: true })
                } else {
                    commit(MessageMutations.CLEAR_MESSAGES)
                    commit(MessageMutations.INCREASE_TIMEOUT_COUNT)
                    showAddTime()
                }
                break;
            case MessageCode.WARNING:
                commit(MessageMutations.CLEAR_MESSAGES)
                break;
            case MessageCode.EMPTY_DEBTS:
                dispatch(MessageModuleActions.CancelTimer, null, { root: true });
                break;
            default:
                dispatch(MessageModuleActions.SendResponse, payload, { root: true })
                break;
        }
    },
    showLoading({ commit }, message) {
        console.log("showLoading")
        var payload = {};
        payload.message = {};
        payload.message.msgType = 4
        payload.message.msgActType = MessageActionTypeEnum.None

        if (message == undefined)
            payload.message.msg = MessageContents.LOADING
        else
            payload.message.msg = message

        if (payload != null) {
            commit(MessageMutations.ADD_MESSAGE, payload);
        }
    },
    showMessage({ commit }, payload) {
        console.log("messagemodule/showMessage : ", payload)
        if (payload != null) {
            commit(MessageMutations.ADD_MESSAGE, payload);
        }
    },
    hideMessage({ commit }, payload) {
        if (payload != null) {
            commit(MessageMutations.DELETE_MESSAGE, payload);
        }
    },
    clearAll({ commit }) {
        commit(MessageMutations.CLEAR_MESSAGES);
    },
    startTimeout({ state, commit, dispatch }, payload) {
        var timerId = setTimeout(() => {
            if (state.timeoutCount > 0 || payload.addTimeStatus) {
                commit(MessageMutations.SET_TIMEOUT_COUNT, 0)
                if (state.timerState.cancel !== undefined) state.timerState.cancel()
                else dispatch(MessageModuleActions.CancelTimer, null, { root: true });
            } else {
                commit(MessageMutations.INCREASE_TIMEOUT_COUNT)
                showAddTime()
            }
        }, payload.time)
        commit(MessageMutations.SET_TIMER, timerId)
    },
    // eslint-disable-next-line no-unused-vars
    stopTimeout({ state, commit }) {
        commit(MessageMutations.SET_TIMEOUT_COUNT, 0)
        if (state.timerId != null) {
            commit(MessageMutations.SET_TIMER, -1)
        }
    },
    reset({ dispatch }) {
        dispatch(MessageModuleActions.StopTimeout, null, { root: true })
    },
    restart({ dispatch }, payload) {
        dispatch(MessageModuleActions.StopTimeout, null, { root: true })
        dispatch(MessageModuleActions.StartTimeout, payload, { root: true })
    },
    cancelTimer() {
        console.log(`default cancelTimer`)

        var body = {};
        body.cmd = ScreenCommands.Timeout;
        body.screen = GlobalKeys.current_screen;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    sendResponse({ commit }, payload) {
        var data = {}
        data.msgResponse = {};
        data.msgResponse.msgCode = payload.msgCode
        data.msgResponse.response = payload.responseState

        var body = {};
        body.cmd = ScreenCommands.ResponseMessage;
        body.screen = GlobalKeys.current_screen;
        body.data = JSON.stringify(data);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}