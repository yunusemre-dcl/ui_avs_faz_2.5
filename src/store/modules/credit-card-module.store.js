import { CommonCommand, CreditCardCommand, ScreenCommands } from "@/enums/command"
import { ScreenEnum } from '@/enums/screen_enum'
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports'
import Vue from "vue"
import { CreditCardMutations } from "../../constants/credit-card-constants"

const state = {
    componentKey: 0,
    title: "",
    totalBill: 0,
    customer: null,
    flagShowAnimation: true,
    infoMessage: "",
    animationCode: 2,
    lblTotalBill: "Toplam Borç",
    nextLabel: "İleri",
    cancelLabel: "İptal",
    prevLabel: "Geri",

    hideBackButton: false,
    hideCancelButton: false,
}
const mutations = {
    [CreditCardMutations.SET_DATA](state, payload) {
        state.hideBackButton = true
        state.hideCancelButton = false
        state.animationCode = payload.animationCode
        state.flagShowAnimation = state.animationCode != 0
        state.infoMessage = payload.message

        state.customer = payload.cust
        state.totalBill = payload.totalBill
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Credit Card] Set Data Mutation`, { payload: payload, state: state })
        console.log("data aktarımı tamamlandı : ", state)
    },
    [CreditCardMutations.CHANGE_DATA](state, payload) {
        state.hideBackButton = true
        state.hideCancelButton = true
        state.animationCode = payload.animationCode
        state.flagShowAnimation = payload.animationCode != 0
        state.infoMessage = payload.message
        state.componentKey++
    },
}
const actions = {
    loadData({ commit }, payload) {
        console.log("Credit Card Load Data : ", payload)
        if (payload != null) {
            commit(CreditCardMutations.SET_DATA, payload.data)
        }
    },
    changeData({ commit }, payload) {
        if (payload != null) {
            commit(CreditCardMutations.CHANGE_DATA, payload.data);
        }
    },
    changeReceived() {
        var body = {};
        body.cmd = CreditCardCommand.CreditCardLoad;
        body.screen = ScreenEnum.CreditCard;
        body.data = "";
        console.log("Change Received: ", body);
        Vue.prototype.$invokeMethod(body);
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.CreditCard;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.CreditCard;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);

    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}