
import { PaybackMutations } from "@/enums/mutations";
import { ScreenEnum } from "@/enums/screen_enum";
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports';
import Vue from "vue";
import { ScreenCommands } from "../../enums/command";

const moduleName = 'paybackmodule'

export const PaybackActions = {
    timeout: `${moduleName}/sendTimeout`
}

const state = {
    componentKey: 1,
    message: "Lütfen para üstünüzü alınız",
    timeout: 10000,
}

const mutations = {
    [PaybackMutations.SET_TIMEOUT](state, payload) {
        state.timeout = payload.timeout;
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Set Timeout Mutation`, {payload: payload, timeout: state.timeout})
    }
}

const actions = {
    // eslint-disable-next-line no-unused-vars
    loadData({commit}, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Load Data`, payload)
    },
    // changeData({commit}, payload) {
    //     LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Load Data`, payload)
    //     commit(PaybackMutations.SET_MESSAGE, payload.data)
    // },
    sendTimeout() {
        var body = {};
        body.cmd = ScreenCommands.PaybackTimeout;
        body.screen = ScreenEnum.Payback;
        body.data = "";
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Payback] Time is up`, body)
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}