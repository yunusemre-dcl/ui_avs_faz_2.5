import { showLoading, showWarning } from "@/constants/message-constants";
import { ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports';
import Vue from "vue";

const moduleName = "loginmodule";

export const LoginActionTypes = {
    LOGIN: `${moduleName}/login`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

const state = {
}
const mutations = {
}
const actions = {
    // eslint-disable-next-line no-unused-vars
    loadData({ commit }, payload) {
        console.log("OpLogin / Load Data ")
    },
    // eslint-disable-next-line no-unused-vars
    login({commit}, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Operator Login] Login Action`, payload)

        if(payload.userTerm === "" || payload.password === "") {
            showWarning("Kullanıcı adı veya şifrenizi eksik tuşladınız")
            return;
        }

        showLoading()

        var loginData = {
            userName: payload.userTerm,
            password: payload.password,
            amount: ""
        }

        var body = {};
        body.cmd = ScreenCommands.OperatorLogin;
        body.screen = ScreenEnum.OperatorLogin;
        body.data = JSON.stringify(loginData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[Operator Login] Login Invoke`, loginData)
    },
    back() {
        var body = {};
        body.cmd = ScreenCommands.Back;
        body.screen = ScreenEnum.CashAccepting;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.DebtQuery;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}