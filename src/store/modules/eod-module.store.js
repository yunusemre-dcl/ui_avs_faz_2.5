import { EodMutations } from "../../constants/eod-constants"

const state = {
    recyclerList: [
        {
            banknot: 5,
            adet: 10,
        },
        {
            banknot: 10,
            adet: 10,
        },
        {
            banknot: 20,
            adet: 1,
        },
        {
            banknot: 50,
            adet: 0,
        }
    ],
    siloList: [
        {
            banknot: 1.00,
            adet: 10,
        },
        {
            banknot: 0.50,
            adet: 10,
        },
        {
            banknot: 0.25,
            adet: 1,
        },
    ],
    kasetList: [
        {
            banknot: 5,
            adet: 10,
        },
    ],
    cashBoxList: [
        {
            banknot: 5,
            adet: 10,
        },
        {
            banknot: 10,
            adet: 10,
        },
        {
            banknot: 20,
            adet: 1,
        },
        {
            banknot: 50,
            adet: 0,
        },
        {
            banknot: 100,
            adet: 0,
        },
        {
            banknot: 200,
            adet: 0,
        }
    ],
    kasetTotal: 0.0,
    cashBoxTotal: 0.0,
    cashTotal: "8.000,00",
    creditTotal: "3.500,29",
    overallTotal: 0.0,
    icmal: 0.0
}

const mutations = {
    // eslint-disable-next-line no-unused-vars
    [EodMutations.SET_LOAD_DATA](state, payload) {
        state.siloList = state.siloList.map(e => {
            var ornek = {
                banknot: e.banknot,
                adet: e.count,
            }

            return ornek
        })
    }
}

const actions = {
    loadData({commit}, payload) {
        commit(EodMutations.SET_LOAD_DATA, payload.data)
    },
    // eslint-disable-next-line no-unused-vars
    print({commit}) {
        // var body = {};
        // body.cmd = ScreenCommands.EksLoadOK;
        // body.screen = ScreenEnum.EksLoad;
        // body.data = "";
        // console.log("Eks Load OK invoke: ", body);
        // Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    process({commit}) {
        // var body = {};
        // body.cmd = ScreenCommands.EksLoadOK;
        // body.screen = ScreenEnum.EksLoad;
        // body.data = "";
        // console.log("Eks Load OK invoke: ", body);
        // Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    menu({commit}) {
        // var body = {};
        // body.cmd = ScreenCommands.EksLoadOK;
        // body.screen = ScreenEnum.EksLoad;
        // body.data = "";
        // console.log("Eks Load OK invoke: ", body);
        // Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}