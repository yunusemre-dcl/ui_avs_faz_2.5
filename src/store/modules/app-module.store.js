import { MessageModuleActions } from '@/constants/message-constants';
import { DebtSelectionCommand, ScreenCommands } from '@/enums/command';
import { ScreenEnum } from '@/enums/screen_enum';
import { Module } from '@/helpers/module';
import { GlobalKeys } from '@/model/global_keys';
import router from '@/router';
import { RoutePaths } from '@/router/route-paths';
import Vue from 'vue';

const that = Vue.prototype;

const state = {}

const mutations = {}

const actions = {
    connectMessage({ dispatch }) {
        that.$hub.on("AppMessage", (jsonData) => {
            var scrModel = JSON.parse(jsonData);
            console.log(scrModel);

            var screen = scrModel.body.screen;
            var cmd = scrModel.body.cmd;

            //#region Data Parsing
            var payload = {}
            payload.data = (scrModel.body.data != null && scrModel.body.data != "") ? JSON.parse(scrModel.body.data) : null;
            console.log(payload)
            //#endregion

            //#region Message Management

            if (GlobalKeys.current_screen != screen) {
                dispatch(MessageModuleActions.ClearMessages);
            }

            var messageData = (cmd == ScreenCommands.ShowMessage) ? JSON.parse(scrModel.body.data) : null;
            if (messageData != null) {
                console.log("Avs Show Message : ", messageData)
                if (messageData.flagHidePrevMsgs != undefined && messageData.flagHidePrevMsgs)
                    dispatch(MessageModuleActions.ClearMessages, null, { root: true })
                dispatch(MessageModuleActions.ShowMessage, messageData, { root: true });
            }
            //#endregion

            //#region GlobalKeys Settings
            GlobalKeys.setClientKey(scrModel.header.client);
            if (screen != 0) GlobalKeys.setCurrentScreen(screen);
            //#endregion

            //#region Screen Management
            switch (screen) {
                case ScreenEnum.Init:
                    console.log("İnit");
                    if (cmd == ScreenCommands.InitLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.Introduction);
                        router.replace({ name: RoutePaths.Introduction });
                    }
                    break;
                case ScreenEnum.Unit:
                    console.log("Unit")
                    console.log("Unit screen  : ", GlobalKeys.current_screen)
                    if (cmd == ScreenCommands.CompanyLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.ProcService);
                        router.replace({ name: RoutePaths.ProcService });
                    }
                    break;
                case ScreenEnum.DebtInquiry:
                    console.log("DebtInquiry")
                    if (cmd == ScreenCommands.InquiryLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.DebtInquiry);
                        router.replace({ name: RoutePaths.DebtInquiry });
                    }
                    break;
                case ScreenEnum.DebtSelection:
                    console.log("DebtSelection");
                    if (cmd == ScreenCommands.DebtSelectionLoad) {
                        GlobalKeys.setCurrentPath(RoutePaths.DebtSelection);
                        router.replace({ name: RoutePaths.DebtSelection }).then(() => {

                            dispatch(Module.setModuleLoad(screen), payload);
                        });
                    } else if (scrModel.body.cmd == DebtSelectionCommand.Changed) {
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
                case ScreenEnum.CashType:
                    console.log("CashType")
                    if (cmd == ScreenCommands.CashTypeLoad && GlobalKeys.current_path != RoutePaths.CashType) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CashType);
                        router.replace({ name: RoutePaths.CashType });
                    }
                    break;
                case ScreenEnum.CashAccepting:
                    console.log("CashAccepting")
                    if (cmd == ScreenCommands.CashAcceptingLoad && GlobalKeys.current_path != RoutePaths.CashAccepting) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CashAccepting);
                        router.replace({ name: RoutePaths.CashAccepting });
                    } else if (cmd == ScreenCommands.CashAcceptingUpdate) {
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
                case ScreenEnum.Payback:
                    console.log("Payback")
                    if (cmd == ScreenCommands.PaybackLoad && GlobalKeys.current_path != RoutePaths.Payback) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.Payback);
                        router.replace({ name: RoutePaths.Payback });
                    }
                    break;
                case ScreenEnum.CreditCard:
                    console.log("CreditCard")
                    if (cmd == ScreenCommands.CreditCardLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CreditCard);
                        router.replace({ name: RoutePaths.CreditCard });
                    } else if (cmd == ScreenCommands.CreditCardChange) {
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
                case ScreenEnum.CardReader:
                    console.log("CardReader")
                    if (cmd == ScreenCommands.CardReaderLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CardReader);
                        router.replace({ name: RoutePaths.CardReader });
                    } else if (cmd == ScreenCommands.CardReaderChange) {
                        dispatch(Module.setModuleChange(screen));
                    }
                    break;
                case ScreenEnum.CalculateCredit:
                    console.log("CalculateCredit")
                    if (cmd == ScreenCommands.CalculateCreditLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.CalculateCredit);
                        router.replace({ name: RoutePaths.CalculateCredit });
                    }
                    break;
                case ScreenEnum.Print:
                    console.log("Print")
                    if (cmd == ScreenCommands.PrintLoad && GlobalKeys.current_path != RoutePaths.Print) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.Print);
                        router.replace({ name: RoutePaths.Print });
                    }
                    break;
                case ScreenEnum.EksLoad:
                    console.log("EksLoad")
                    console.log("Eks body cmd : ", scrModel.body.cmd)
                    if (cmd == ScreenCommands.EksLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.EksLoad);
                        router.replace({ name: RoutePaths.EksLoad });
                    }
                    // else if (scrModel.body.cmd == ScreenCommands.EksChange) {
                    //     dispatch(Module.setModuleChange(screen), payload);
                    // }
                    break;
                case ScreenEnum.OperatorLogin:
                    console.log("OperatorLogin")
                    if (cmd == ScreenCommands.OperatorLoginLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.OpLogin);
                        router.replace({ name: RoutePaths.OpLogin });
                    }
                    break;
                case ScreenEnum.OperatorMenu:
                    console.log("OperatorMenu")
                    if (cmd == ScreenCommands.OperatorMenuLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.OpMenu);
                        router.replace({ name: RoutePaths.OpMenu });
                    }
                    break;
                case ScreenEnum.OperatorCoin:
                    console.log("OperatorCoin")
                    if (cmd == ScreenCommands.OperatorSetSiloLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.OpSilo);
                        router.replace({ name: RoutePaths.OpSilo });
                    }
                    break;
                case ScreenEnum.OperatorMakbuz:
                    console.log("OperatorMakbuz")
                    if (cmd == ScreenCommands.OperatorMakbuzLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.OpMakbuz);
                        router.replace({ name: RoutePaths.OpMakbuz });
                    }
                    break;
                case ScreenEnum.OperatorCashDispense:
                    console.log("OperatorCashDispense")
                    if (cmd == ScreenCommands.OperatorSetRecyclerLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.OpRecycler);
                        router.replace({ name: RoutePaths.OpRecycler });
                    } else {
                        payload.state = cmd
                        dispatch(Module.setModuleChange(screen), payload);
                    }
                    break;
                case ScreenEnum.OperatorReport:
                    console.log("OperatorReport")
                    if (cmd == ScreenCommands.OperatorReportLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.OpMenu);
                        router.replace({ name: RoutePaths.OpMenu });
                    }
                    break;
                case ScreenEnum.OperatorAlarm:
                    console.log("OperatorAlarm")
                    if (cmd == ScreenCommands.OperatorAlarmLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.OpMenu);
                        router.replace({ name: RoutePaths.OpMenu });
                    }
                    break;
                case ScreenEnum.OperatorKaset:
                    console.log("OperatorKaset")
                    if (cmd == ScreenCommands.OperatorKasetLoad) {
                        dispatch(Module.setModuleLoad(screen), payload);
                        GlobalKeys.setCurrentPath(RoutePaths.OpMenu);
                        router.replace({ name: RoutePaths.OpMenu });
                    }
                    break;

            }
            //#endregion
        });
    },
}

export default {
    state,
    mutations,
    actions,
}