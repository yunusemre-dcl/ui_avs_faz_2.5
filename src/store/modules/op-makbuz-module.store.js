import { showLoading, showWarning } from "@/constants/message-constants";
import { OpMakbuzMutations } from "@/constants/op-makbuz-constants";

const state = {
    makbuzList: [
        {
            tarih: "24.12.2021 11:25:25:2545",
            aboneNo: "102570",
            makbuzNo: "605386214-12525",
        },
        {
            tarih: "12.12.12.12.12",
            aboneNo: "12312312312312",
            makbuzNo: "25231269964",
        },
        {
            tarih: "12.12.12.12.12",
            aboneNo: "12312312312312",
            makbuzNo: "2523126996",
        }
    ],
    selectedMakbuzNo: 0,
    logList: [
        "Yükleme işlemi başlatıldı",
        "Yükleme işlemi tamamlandı",
    ],
}

const mutations = {
    [OpMakbuzMutations.SET_SELECTED_MAKBUZ](state, payload) {
        state.selectedMakbuzNo = payload.makbuzNo
    }
}

const actions = {
    selectMakbuz({ commit }, payload) {
        commit(OpMakbuzMutations.SET_SELECTED_MAKBUZ, payload)
    },
    // eslint-disable-next-line no-unused-vars
    submit({ commit }, payload) {

        if (payload.aboneNo === "" || payload.tarih === "") {
            showWarning("Abone no ve/veya tarih boş bırakılamaz");
            return;
        }

        showLoading()
        console.log("submit : ", payload)
        // var body = {};
        // body.cmd = ScreenCommands.SiloUpdate;
        // body.screen = ScreenEnum.Silo;
        // body.data = "";
        // console.log(body);
        // Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    print({ commit }, payload) {
        // var body = {};
        // body.cmd = ScreenCommands.SiloUpdate;
        // body.screen = ScreenEnum.Silo;
        // body.data = "";
        // console.log(body);
        // Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}