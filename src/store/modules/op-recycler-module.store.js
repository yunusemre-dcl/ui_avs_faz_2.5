/* eslint-disable no-unused-vars */
import { CashDispenseMutations } from "../../constants/cash-dispense-constants"
import { ScreenCommands } from '@/enums/command';
import { ScreenEnum } from '@/enums/screen_enum';
import Vue from "vue";

const state = {
    componentKey: 0,
    banknotes: [],
    kioskName: "",
    logList: [],

    // Başlat, Bitir, Reset state
    btnBNRStartEnabled: true,
    btnBNRStopEnabled: true,
    btnBNRResetEnabled: true,

    // Menu, İleri, Geri state
    btnMenuEnabled: true,
    btnNextEnabled: true,
    btnBackEnabled: true,
}

const mutations = {
    [CashDispenseMutations.SET_LOAD_DATA](state, payload) {
        console.log("cash-dispense MUT ADD_LOG : ", payload)
        state.banknotes = payload.banknotes
        state.kioskName = payload.kioskName
        state.btnBNRStopEnabled = payload.btnBNRStopEnabled
    },
    [CashDispenseMutations.ADD_LOG](state, payload) {
        console.log("cash-dispense MUT ADD_LOG : ", payload)
        if (payload.clear !== undefined && payload.clear) {
            state.logList = []
        }
        state.logList = [...state.logList, payload.message]
        state.componentKey++
    },
    [CashDispenseMutations.UPDATE_BANKNOTES](state, payload) {
        console.log("cash-dispense MUT UPDATE_BANKNOTES : ", payload)
        state.banknotes = payload.banknotes
        console.log("cash-dispense MUT UPDATED_BANKNOTES : ", state.banknotes)
        state.componentKey++
    },
    [CashDispenseMutations.UPDATE_RESET_BUTTON_STATE](state, payload) {
        console.log("cash-dispense MUT UPDATE_RESET_BUTTON_STATE : ", payload)
        state.btnBNRStartEnabled = payload.btnBNRStartEnabled
        state.btnBNRStopEnabled = payload.btnBNRStopEnabled
        state.btnBNRResetEnabled = payload.btnBNRResetEnabled
        state.componentKey++
    },
    [CashDispenseMutations.UPDATE_MENU_BUTTON_STATE](state, payload) {
        console.log("cash-dispense MUT UPDATE_MENU_BUTTON_STATE : ", payload)
        state.btnMenuEnabled = payload.btnMainMenuEnabled
        state.btnNextEnabled = payload.btnForwardEnabled
        state.btnBackEnabled = payload.btnBackwardEnabled
        state.componentKey++
    },
    [CashDispenseMutations.UPDATE_END_BUTTON_STATE](state, payload) {
        console.log("cash-dispense MUT UPDATE_END_BUTTON_STATE : ", payload)
        state.btnBNRStopEnabled = payload.btnBNRStopEnabled
        state.componentKey++
    },
}

const actions = {
    loadData({ commit }, payload) {
        console.log("op-recycler/loadData : ", payload)
        commit(CashDispenseMutations.SET_LOAD_DATA, payload.data)
    },
    changeData({ commit }, payload) {
        console.log("op-recycler/changeData : ", payload)
        switch (payload.state) {
            case ScreenCommands.OperatorSetRecyclerChanged:
                commit(CashDispenseMutations.UPDATE_BANKNOTES, payload.data)
                break;
            case ScreenCommands.OperatorSetRecyclerAppendLog:
                commit(CashDispenseMutations.ADD_LOG, payload.data)
                break;
            // Bir çok button
            case ScreenCommands.OperatorSetRecyclerResetButtonState:
                commit(CashDispenseMutations.UPDATE_RESET_BUTTON_STATE, payload.data)
                break;
            // Menu, İleri, Geri
            case ScreenCommands.OperatorSetRecyclerMenuButtonState:
                commit(CashDispenseMutations.UPDATE_MENU_BUTTON_STATE, payload.data)
                break;
            // Bitir butonu
            case ScreenCommands.OperatorSetRecyclerEndButtonState:
                commit(CashDispenseMutations.UPDATE_END_BUTTON_STATE, payload.data)
                break;
            default:
                break;
        }
    },
    buttonState({ commit }, payload) {
        var data = {}
        data.buttonState = payload

        var body = {};
        body.cmd = ScreenCommands.OperatorSetRecyclerSelect;
        body.screen = ScreenEnum.OperatorCashDispense;
        body.data = JSON.stringify(data);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}