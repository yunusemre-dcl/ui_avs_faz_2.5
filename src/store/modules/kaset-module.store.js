import { KasetMutations } from '@/constants/kaset-constants';
import { ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import Vue from 'vue';

const state = {
    kasetList: [
        {
            unite: 1,
            birim: 5,
            ilkDeger: 10,
            mevcut: 10,
            yeni: 0,
        },

    ], // Eğer kasetler AVS den dinamik olarak geliyorsa bu liste kullanılacak
    selectedUnite: 1,
}

const mutations = {
    [KasetMutations.SET_KASET_LIST](state, payload) {
        state.kasetList = payload
    },
    [KasetMutations.SET_SELECTED_UNITE](state, payload) {
        state.selectedUnite = payload
    },
    [KasetMutations.UPDATE_KASET](state, payload) {
        state.kasetList = state.kasetList.filter(kaset => {
            if (kaset.unite === state.selectedUnite) kaset.yeni = payload
            return kaset
        })
    },
}

const actions = {
    loadData({ commit }, payload) {
        console.log("kaset loadData")
        if (payload != null) {
            commit(KasetMutations.SET_KASET_LIST, payload.data)
        }
    },
    // eslint-disable-next-line no-unused-vars
    changeData({ commit }, data) {
        //commit(InquiryModuleMutations.SET_VALUE, data)
    },
    selectUnite({ commit }, payload) {
        commit(KasetMutations.SET_SELECTED_UNITE, payload)
    },
    updateKaset({ commit }, payload) {
        commit(KasetMutations.UPDATE_KASET, payload)
    },
    // eslint-disable-next-line no-unused-vars
    save({commit}) {
        var body = {};
        body.cmd = ScreenCommands.KasetUpdate;
        body.screen = ScreenEnum.Kaset;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}