import { CashTypeMutations } from "@/constants/cash-type-constants";
import { CommonCommand, ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports';
import Vue from "vue";

const state = {
    componentKey: 1,
    payTypes: [],
    title: "Lütfen ödeme tipini seçiniz",
    backButtonVisible: true,
    nextLabel: "İleri",
    prevLabel: "Geri",
    cancelLabel: "İptal",
}

const mutations = {
    [CashTypeMutations .SET_PAY_TYPES](state, payload) {
        //state.backButtonVisible = payload.backButtonVisible
        state.payTypes = payload.cashTypes;
        state.componentKey++
    },
}

const actions = {
    loadData({commit}, payload){
        if(payload != null) {
            commit(CashTypeMutations.SET_PAY_TYPES, payload.data)
        }
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.CashType;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH TYPE] BACK`, body)
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    cashTypeSelected({commit}, selectedType){
        var inSelectedPayTypeData = {};
        inSelectedPayTypeData.cashTypeId = selectedType.cashTypeId;

        var body = {};
        body.cmd = ScreenCommands.CashTypeSelect;
        body.screen = ScreenEnum.CashType;
        body.data = JSON.stringify(inSelectedPayTypeData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.CashType;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH TYPE] CANCEL`, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}