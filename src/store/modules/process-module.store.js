
import { showLoading } from "@/constants/message-constants";
import { ProcessCompanyGroups, ProcessModuleMutations } from "@/constants/process-constants";
import { CommonCommand, ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import Vue from "vue";

const state = {
    title: "Lütfen işlem tipini seçiniz",
    nextLabel: "İleri",
    prevLabel: "Geri",
    cancelLabel: "İptal",
    svcs: [
        {
            CompanyId: 1,
            Name: "Test 1",
        },
        {
            CompanyId: 2,
            Name: "Test 2",
        },
        {
            CompanyId: 3,
            Name: "Test 3",
        }
    ],
}

const mutations = {
    [ProcessModuleMutations.SET_SVCS](state, payload) {
        state.svcs = [];

        var grouplessList = payload.filter(e => !(e.Group > 0))
        grouplessList.forEach(element => {
            state.svcs.push(element)
        });

        var eksGroup = payload.filter(e => e.Group === ProcessCompanyGroups.EKS)
        if (eksGroup.length > 0) {
            var anyActiveSvc = (eksGroup.filter(e => e.Status == 1).length) > 0
            eksGroup[0].Name = eksGroup[0].GroupName
            eksGroup[0].Status = anyActiveSvc ? 1 : 0
            state.svcs.push(eksGroup[0])
        }
    },
}

const actions = {
    loadData({ commit }, payload) {
        console.log("Selection Module Load Data : ", payload)
        if (payload != null) {
            commit(ProcessModuleMutations.SET_SVCS, payload.data)
        }
    },
    changeData({ commit }, payload) {
        if (payload != null) {
            commit(ProcessModuleMutations.SET_SVCS, payload.data)
        }
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.Unit;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.Unit;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    select({ commit }, selectedProcService) {
        showLoading()

        var inSelectedUnitData = {};
        inSelectedUnitData.CompanyId = selectedProcService.CompanyId;
        inSelectedUnitData.Group = selectedProcService.Group;

        if (selectedProcService.CompanyId < 499)
            inSelectedUnitData.NextScreen = ScreenEnum.DebtInquiry
        else
            inSelectedUnitData.NextScreen = ScreenEnum.CardReader

        var body = {};
        body.cmd = ScreenCommands.CompanySelection;
        body.screen = ScreenEnum.Unit;
        body.data = JSON.stringify(inSelectedUnitData);
        console.log("procmdoule body : ", body);
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}