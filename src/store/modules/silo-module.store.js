import { SiloMutations } from '@/constants/silo-constants';
import { ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import Vue from 'vue';

const state = {
    siloList: [], // Eğer silolar AVS den dinamik olarak geliyorsa bu liste kullanılacak
    selectedUnite: 1,
}

const mutations = {
    [SiloMutations.SET_SILO_LIST](state, payload) {
        console.log("op-silo SET_SILO_LIST MUT : ", payload)
        state.siloList = payload.coins.map((silo, index) => {
            silo.unite = index+1
            return silo
        })
    },
    [SiloMutations.SET_SELECTED_UNITE](state, payload) {
        console.log("op-silo SET_SELECTED_UNITE MUT : ", payload)
        state.selectedUnite = payload
    },
    [SiloMutations.UPDATE_SILO](state, payload) {
        console.log("op-silo UPDATE_SILO MUT : ", payload)
        state.siloList = state.siloList.filter(silo => {
            if (silo.unite === state.selectedUnite) silo.yeni = payload
            return silo
        })
    },
}

const actions = {
    loadData({ commit }, payload) {
        console.log("silo loadData")
        if (payload != null) {
            commit(SiloMutations.SET_SILO_LIST, payload.data)
        }
    },
    // eslint-disable-next-line no-unused-vars
    changeData({ commit }, data) {
        commit(SiloMutations.SET_SILO_LIST, data)
    },
    selectUnite({ commit }, payload) {
        commit(SiloMutations.SET_SELECTED_UNITE, payload)
    },
    updateSilo({ commit }, payload) {
        commit(SiloMutations.UPDATE_SILO, payload)
    },
    // eslint-disable-next-line no-unused-vars
    save({state, commit}) {
        var data = {}
        data.coins = state.siloList.map(e => {
            var newSilo = {}
            newSilo.value = e.value
            newSilo.initial = e.initial
            newSilo.present = e.present
            newSilo.yeni = e.yeni

            return newSilo
        })

        var body = {};
        body.cmd = ScreenCommands.OperatorSetSiloSave;
        body.screen = ScreenEnum.OperatorCoin;
        body.data = JSON.stringify(data);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    buttonState({ commit }, payload) {
        var data = {}
        data.buttonState = payload

        var body = {};
        body.cmd = ScreenCommands.OperatorSetRecyclerSelect;
        body.screen = ScreenEnum.OperatorCashDispense;
        body.data = JSON.stringify(data);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}