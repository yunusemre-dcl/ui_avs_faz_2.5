import { InquiryModuleMutations } from "@/constants/inquiry-constants";
import { showLoading } from "@/constants/message-constants";
import { CommonCommand, ScreenCommands } from "@/enums/command";
import { ScreenEnum } from '@/enums/screen_enum';
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports';
import Vue from "vue";

const defaultState = {
    //componentKey: 0,
    title: "Lütfen faturanızdaki barkodu okutunuz veya numaranızı giriniz",
    queryTypes: [
        {
            id: 1,
            name: "Abone No",
            isDefault: true,
        },
        {
            id: 2,
            name: "Sicil No",
            isDefault: false,
        }
     ],
    // queryTypes: [],
    selectedQueryType: {
        id: 1,
        name: "Abone No",
        isDefault: true,
    },
    // selectedQueryType: null,
    value: "",
    nextButtonDisable: true,
    nextLabel: "İleri",
    prevLabel: "Geri",
    cancelLabel: "İptal",
}

const state = {
    ...defaultState
};

const mutations = {
    // Ekranın ilk yüklenmesinde çalışır. Listeler doldurulur.
    [InquiryModuleMutations.SET_QUERY_TYPES](state, payload) {
        state.nextButtonDisable = true
        state.value = ""
        state.queryTypes = []
        payload.queryTypes.forEach(qType => {
            state.queryTypes.push(qType)
        })

        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Set Data Mutation QueryTypes`, state.queryTypes)

        state.selectedQueryType = payload.queryTypes.find(
            (qtype) => qtype.isDefault == true
        );
        console.log("state : ", state)
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Set Data Mutation Selected Query`, state.selectedQueryType)
    },
    // Seçili inquiryType değerini günceller
    [InquiryModuleMutations.SET_SELECTED_QTYPE](state, payload) {
        state.nextButtonDisable = true
        state.value = ""
        state.selectedQueryType = payload
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Set Selected QueryType`, state.selectedQueryType)
    },
    [InquiryModuleMutations.SET_VALUE](state, payload) {
        state.nextButtonDisable = false
        console.log('value değişiyor : ', payload)

        var currentLength = state.value.length
        var limit = 12

        if (currentLength == limit) return;

        state.value += payload.value.toString();
    },
    // Silme butonuna basılınca value'dan birer birer silme işlemini gerçekleştirir.
    [InquiryModuleMutations.SET_VALUE_CLEAR](state) {
        if (state.value.length > 0)
            state.value = state.value.slice(0, -1);

        if(state.value.length == 0) state.nextButtonDisable = true

    },
    [InquiryModuleMutations.RESET_VALUE](state) {
        state.nextButtonDisable = true
        state.value = "";
    },
    // eslint-disable-next-line no-unused-vars
    [InquiryModuleMutations.RESET](state) {
        state = {
            ...defaultState
        };
        //state.componentKey++
    },
}

const actions = {
    loadData({ commit }, payload) {
        console.log("inquiryModule loadData")
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] Load Data Action`, payload)
        if (payload != null) {
            commit(InquiryModuleMutations.RESET)
            commit(InquiryModuleMutations.SET_QUERY_TYPES, payload.data)
        }
    },
    changeValue({ commit }, data) {
        commit(InquiryModuleMutations.SET_VALUE, data)
    },
    selectedQtype({ commit }, data) {
        commit(InquiryModuleMutations.SET_SELECTED_QTYPE, data)
    },
    clearValue({ commit }) {
        commit(InquiryModuleMutations.SET_VALUE_CLEAR)
    },
    resetValue({commit}) {
        commit(InquiryModuleMutations.RESET_VALUE)
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.DebtInquiry;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] BACK`, body)
        Vue.prototype.$invokeMethod(body);
    },
    // eslint-disable-next-line no-unused-vars
    next({ state, commit }) {
        showLoading()
        commit(InquiryModuleMutations.RESET)

        var selectedQueryTypeModel = {};
        selectedQueryTypeModel.qTypeId = state.selectedQueryType.id
        selectedQueryTypeModel.value = state.value

        console.log(selectedQueryTypeModel);
        var body = {};
        body.cmd = ScreenCommands.InquirySelect;
        body.screen = ScreenEnum.DebtInquiry;
        body.data = JSON.stringify(selectedQueryTypeModel);
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] NEXT`, body)
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.DebtInquiry;
        body.data = "";
        console.log(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[DEBT INQUIRY] CANCEL`, body)
        Vue.prototype.$invokeMethod(body);
    }
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}