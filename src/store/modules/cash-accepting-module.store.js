import { CashAcceptingActions, CashAcceptingMutations } from "@/constants/cash-accepting-constants"
import { CommonCommand, ScreenCommands } from "@/enums/command"
import { ScreenEnum } from '@/enums/screen_enum'
import { LogLevelEnum, LogPriorityEnum, LogService } from '@/plugins/log_exports'
import Vue from "vue"
import { MessageContents, showLoading } from "../../constants/message-constants"

const state = {
    componentKey: 0,
    infoMessage: "Lütfen ışık yanınca para girişinden paraları tek tek veriniz",
    totalBill: 0.0,
    accepted: 0.0,
    customer: {
        displayName: "",
        qTypeName: "",
        queryNo: "",  
    },
    cashModels: [],
    flagDetail: false,
    acceptedLabel: "Yatırılan Tutar",
    lblTotalBill: "Ödenecek Tutar",
    clmCount: "",
    clmTotal: "",
    clmValue: "",
    nextLabel: "İleri",
    prevLabel: "Geri",
    cancelLabel: "İptal",
    showPrevButton: false,
    showAnim: false,
}

const mutations = {
    [CashAcceptingMutations.SET_DATA](state, payload) {
        state.flagDetail = false
        state.totalBill = payload.totalBill
        state.accepted = payload.accepted
        state.customer = payload.cust
        state.showPrevButton = true
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Load Data Mutation`, {payload: payload, state: state})
    },
    [CashAcceptingMutations.CHANGE_DATA](state, payload) {
        state.accepted = payload.accepted;
        state.showPrevButton = false
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Change Data Mutation`, {payload: payload, state: state})
    },
    // [CashAcceptingMutations.SET_CASH_MODELS](state, payload) {
    //     state.cashModels = [];
    //     var banknoteModels = [];
    //     var coinModels = [];

    //     if (payload.acceptedBanknotes != null) {
    //         banknoteModels = [...payload.acceptedBanknotes].map((banknote) => {
    //             return {
    //                 count: banknote.count,
    //                 total: banknote.total,
    //                 type: banknote.type.value,
    //             };
    //         });
    //     }
    //     if (payload.acceptedCoins != null) {
    //         coinModels = [...payload.acceptedCoins].map((coin) => {
    //             return {
    //                 count: coin.count,
    //                 total: coin.total,
    //                 type: coin.type.value,
    //             };
    //         });
    //     }
    //     state.cashModels = [...banknoteModels, ...coinModels];
    //     LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Set Cash Models`, {payload: payload, cashModels: state.cashModels})
    // },
    [CashAcceptingMutations.REFRESH](state) {
        state.componentKey++
    }
}

const actions = {
    loadData({ commit }, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Load Data Action`, payload)
        if (payload != null) {
            commit(CashAcceptingMutations.SET_DATA, payload.data)
        }
    },
    changeData({ state, commit, dispatch }, payload) {
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Change Data Action`, payload)
        if (payload != null) {
            commit(CashAcceptingMutations.CHANGE_DATA, payload.data);
            commit(CashAcceptingMutations.REFRESH)
            
            if(state.accepted >= state.totalBill) {
                dispatch(CashAcceptingActions.PAYMENT_RECEIVED, null, {root: true})
            }
        }
        
        //dispatch("changeReceived")

        // var body = {};
        // body.cmd = CashAcceptingCommand.ChangeReceived;
        // body.screen = ScreenEnum.CashAccepting;
        // body.data = "";
        // console.log("Change Received: ", body);
        // Vue.prototype.$invokeMethod(body);
    },
    paymentReceived() {
        showLoading(MessageContents.LOADING_PROCCESS)

        console.log("paymentReceived started")
        var paymentData = {}
        paymentData.selection = 3

        var body = {};
        body.cmd = ScreenCommands.CashAcceptingComplete
        body.screen = ScreenEnum.CashAccepting
        body.data = JSON.stringify(paymentData);
        console.log("Change Received: ", body);
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Change Received`, body)
    },
    back() {
        var body = {};
        body.cmd = CommonCommand.Back;
        body.screen = ScreenEnum.CashAccepting;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Back`, body)
    },
    cancel() {
        console.log("cancel started")
        var cancelData = {}
        cancelData.selection = -1

        var body = {};
        body.cmd = ScreenCommands.CashAcceptingComplete;
        body.screen = ScreenEnum.CashAccepting;
        body.data = JSON.stringify(cancelData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
        LogService.pushLog(LogPriorityEnum.Info, LogLevelEnum.L4, `[CASH ACCEPTING] Cancel`, body)
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}