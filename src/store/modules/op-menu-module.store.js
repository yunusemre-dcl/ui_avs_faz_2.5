import { OpMenuMutations } from "@/constants/menu-constants";
import { showLoading } from "@/constants/message-constants";
import { ScreenCommands } from '@/enums/command';
import { ScreenEnum } from '@/enums/screen_enum';
import Vue from "vue";
import { OpMenuEnum } from "@/enums/op_menu_enum";

const state = {
    svcs: [
        {
            id: OpMenuEnum.Report,
            name: "Rapor",
            state: true,
        },
        {
            id: OpMenuEnum.Set,
            name: "Set Ekranı",
            state: true,
        },
        {
            id: OpMenuEnum.Alarms,
            name: "Alarmlar",
            state: true,
        },
        {
            id: OpMenuEnum.Test,
            name: "Test Ekranı",
            state: true,
        },
        {
            id: OpMenuEnum.Receipt,
            name: "Makbuz",
            state: true,
        },
        {
            id: OpMenuEnum.Maintenance,
            name: "Bakım ve Onarım",
            state: true,
        },
    ],
    operatorName: "",
    btnMakbuzVisible: true,
    btnSetVisible: true,
    btnReportVisible: true,
    btnDeviceMaintenanceVisible: false
}

const mutations = {
    // eslint-disable-next-line no-unused-vars
    [OpMenuMutations.SET_OPERATOR](state, payload) {
         console.log("op-menu/SET_OPERATOR : ", payload)
         console.log("op-menu MUT before state : ", state)
        state.operatorName = payload.operatorName
        state.svcs = state.svcs.filter(e => {
            switch (e.id) {
                case 1:
                    e.state = payload.btnReportVisible
                    break;
                case 2:
                    e.state = payload.btnSetVisible
                    break;
                case 5:
                    e.state = payload.btnMakbuzVisible
                    break;
                case 6:
                    e.state = payload.btnDeviceMaintenanceVisible
                    break;
                default:
                    break;
            }

            return e.state
        })
        console.log("op-menu MUT state : ", state)
    }
}

const actions = {
    loadData({ commit }, payload) {
        console.log("op-menu/loadData : ", payload)
        commit(OpMenuMutations.SET_OPERATOR, payload.data)
    },
    // eslint-disable-next-line no-unused-vars
    select({ commit }, selectedProcService) {
        showLoading()
        var inSelectedUnitData = {};
        // AVS e gönderilecek model belirlenince svcId güncellenecek
        inSelectedUnitData.menuButton = selectedProcService.id;

        var body = {};
        body.cmd = ScreenCommands.OperatorMenuSelect;
        body.screen = ScreenEnum.OperatorMenu;
        body.data = JSON.stringify(inSelectedUnitData);
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    back() {
        console.log("back tıklandı");

        var body = {};
        body.cmd = ScreenCommands.Back;
        body.screen = ScreenEnum.OperatorMenu;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
    cancel() {
        var body = {};
        body.cmd = ScreenCommands.Cancel;
        body.screen = ScreenEnum.OperatorMenu;
        body.data = "";
        console.log(body);
        Vue.prototype.$invokeMethod(body);
    },
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}