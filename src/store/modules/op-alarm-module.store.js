const state = {
   alarmList: [
      {
         priority: "Majör",
         alarm: "Bozuk para verme ünitesi bozuldu.",
         date: "12.31.2021 11:23",
         source: "Bozuk Para ünitesi"
      },
      {
         priority: "Minor",
         alarm: "Nakit para verme ünitesi bozuldu.",
         date: "12.31.2021 11:23",
         source: "Bozuk Para ünitesi"
      },
      {
         priority: "Keyfim",
         alarm: "Keyfim para verme ünitesi bozuldu.",
         date: "12.31.2021 11:23",
         source: "Bozuk Para ünitesi"
      },
   ]
}

const mutations = {}

const actions = {}

export default {
   state,
   mutations,
   actions,
   namespaced: true,
}