


const moduleName = "initializemodule";

export const InitializeActionTypes = {
    UPDATED: `${moduleName}/updated`,
    LOAD: `${moduleName}/load`,
    CANCEL: `${moduleName}/completed`,
}

const state = {
}

const mutations = {
}

const actions = {
}

export default {
    state,
    mutations,
    actions,
    namespaced: true,
}