import { ScreenType } from '@/enums/screen_type';
import { GlobalKeys } from '@/model/global_keys';
import Vue from 'vue';
import Vuex from 'vuex';
// import the auto exporter
import modules from './modules';

Vue.use(Vuex)

const SET_SCREEN_TYPE = "setScreenType";

const store = new Vuex.Store({
  state: {
    screenType: ScreenType.Landscape,
  },
  mutations: {
    [SET_SCREEN_TYPE](state, payload) {
      state.screenType = payload;
    }
  },
  actions: {
    getScreenType({ commit }, payload) {
      if (payload > 1.0) commit(SET_SCREEN_TYPE, ScreenType.Landscape);
      else commit(SET_SCREEN_TYPE, ScreenType.Portrait);
    },
    // eslint-disable-next-line no-unused-vars
    invoke({commit}, payload) {
      var body = {};
      body.cmd = payload.cmd;
      body.screen = payload.screen;
      body.data = "";
      console.log(body);
      Vue.prototype.$invokeMethod(body);
    },
    reset({ commit }) {
      // resets state of all the modules
      Object.keys(modules).forEach(moduleName => {
        moduleName.state = moduleName.defaultState
        commit(`${moduleName}/RESET`);
      })
    }
  },
  modules,
})

GlobalKeys.setDispatch(store.dispatch)

export default store
