import Vue from 'vue';
import VueRouter from 'vue-router';
import { RoutePaths } from './route-paths';

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: () => import('@/pages/Home.vue')
  },
  {
    path: '/',
    name: RoutePaths.Initialize,
    component: () => import('@/pages/Initialize.vue')
  },
  {
    path: '/intro',
    name: RoutePaths.Introduction,
    component: () => import('@/pages/Introduction.vue')
  },
  {
    path: '/proc-service',
    name: RoutePaths.ProcService,
    component: () => import('@/pages/ProcessService.vue')
  },
  {
    path: '/inquiry',
    name: RoutePaths.DebtInquiry,
    component: () => import('@/pages/DebtInquiry.vue')
  },
  {
    path: '/debt-selection',
    name: 'debt-selection',
    component: () => import('@/pages/DebtList.vue')
  },
  {
    path: '/cash-type',
    name: 'cash-type',
    component: () => import('@/pages/CashTypeSelection.vue')
  },
  {
    path: '/cash-accepting',
    name: RoutePaths.CashAccepting,
    component: () => import('@/pages/CashAccepting.vue')
  },
  {
    path: '/payback',
    name: RoutePaths.Payback,
    component: () => import('@/pages/Payback.vue')
  },
  {
    path: '/credit-card',
    name: RoutePaths.CreditCard,
    component: () => import('@/pages/CreditCard.vue')
  },
  {
    path: '/print',
    name: RoutePaths.Print,
    component: () => import('@/pages/Print.vue')
  },
  {
    path: '/card-reader',
    name: RoutePaths.CardReader,
    component: () => import('@/pages/CardReader.vue')
  },
  {
    path: '/calculate-credit',
    name: RoutePaths.CalculateCredit,
    component: () => import('@/pages/CalculateCredit.vue')
  },
  {
    path: '/eks-load',
    name: RoutePaths.EksLoad,
    component: () => import('@/pages/EksLoad.vue')
  },
  {
    path: '/op-login',
    name: RoutePaths.OpLogin,
    component: () => import('@/pages/OpLogin.vue')
  },
  {
    path: '/op-menu',
    name: RoutePaths.OpMenu,
    component: () => import('@/pages/OpMenu.vue')
  },
  {
    path: '/recycler',
    name: RoutePaths.OpRecycler,
    component: () => import('@/pages/OpRecycler.vue')
  },
  {
    path: '/silo',
    name: RoutePaths.OpSilo,
    component: () => import('@/pages/OpSilo.vue')
  },
  {
    path: '/op-makbuz',
    name: RoutePaths.OpMakbuz,
    component: () => import('@/pages/OpMakbuz.vue')
  },
  {
    path: '/op-kaset',
    name: RoutePaths.OpKaset,
    component: () => import('@/pages/OpKaset.vue')
  },
  {
    path: '/eod',
    name: RoutePaths.Eod,
    component: () => import('@/pages/EOD.vue')
  },
  {
    path: '/op-alarm',
    name: RoutePaths.OpAlarm,
    component: () => import('@/pages/OpAlarm.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
