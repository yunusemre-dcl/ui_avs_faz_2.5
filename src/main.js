import App from '@/components/App.vue';
import '@/components/_globals';
import LottieAnimation from "lottie-vuejs/src/LottieAnimation.vue";
import Vue from 'vue';
import './helpers/extensions';
//import LogSignalRPlugin from './plugins/log_signalr_plugin';
import SignalRPlugin from './plugins/signalr_plugin';
import vuetify from './plugins/vuetify';
import router from './router';
import store from './store';


Vue.config.productionTip = false

Vue.use(SignalRPlugin);
//Vue.use(LogSignalRPlugin);
Vue.use(LottieAnimation);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
