const moduleName = "eksloadmodule";

export const EksLoadActions = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

export const EksLoadMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
}