const moduleName = "opmenumodule";

export const OpMenuModuleActions = {
    SELECT: `${moduleName}/select`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
};

export const OpMenuMutations = {
    SET_OPERATOR : 'setOperator',
}