const moduleName = "silomodule";

export const SiloMutations = {
    SET_SILO_LIST: 'setSiloList',
    SET_SELECTED_UNITE: 'setSelectedUnit',
    UPDATE_SILO: 'updateSilo',
}

export const SiloActions = {
    SELECT_UNITE: `${moduleName}/selectUnite`,
    UPDATE_SILO: `${moduleName}/updateSilo`,
    SAVE: `${moduleName}/save`,
    BUTTON_STATE: `${moduleName}/buttonState`,
}

export const BtnTypes = {
    START: 1,
    STOP: 2,
    RESET: 3,
    NEXT: 4,
    BACK: 5,
    MENU: 6,
}