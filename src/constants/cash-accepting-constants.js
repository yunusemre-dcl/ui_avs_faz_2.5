const moduleName = "cashacceptingmodule";

export const CashAcceptingActions = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    PAYMENT_RECEIVED: `${moduleName}/paymentReceived`,
}

export const CashAcceptingMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    REFRESH: 'refresh',
}