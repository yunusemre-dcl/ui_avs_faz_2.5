import { GlobalKeys } from '../model/global_keys';
const moduleName = "messagemodule";

export const MessageModuleActions = {
    ModuleName: moduleName,
    LoadTimerState: `${moduleName}/loadTimerState`,
    ShowLoading: `${moduleName}/showLoading`,
    HideMessage: `${moduleName}/hideMessage`,
    ShowMessage: `${moduleName}/showMessage`,
    ClearMessages: `${moduleName}/clearAll`,
    StartTimeout: `${moduleName}/startTimeout`,
    StopTimeout: `${moduleName}/stopTimeout`,
    Reset: `${moduleName}/reset`,
    Restart: `${moduleName}/restart`,
    CancelTimer: `${moduleName}/cancelTimer`,
    SendResponse: `${moduleName}/sendResponse`,
}

export const MessageMutations = {
    ADD_MESSAGE: 'addMessage',
    DELETE_MESSAGE: 'deleteMessage',
    CLEAR_MESSAGES: 'clearMessages',
    INCREASE_TIMEOUT_COUNT: 'increaseTimeCount',
    SET_TIMER: 'setTimer',
    SET_TIMEOUT_COUNT: 'setTimeoutCount',
    SET_TIMER_STATE: 'setTimerState',
}

export const MessageContents = {
    LOADING: "Lütfen bekleyiniz",
    LOADING_PROCCESS: "İşleminiz yapılıyor.\nLütfen bekleyiniz",
    LOADING_CANCEL: "İşleminiz iptal ediliyor.\nLütfen bekleyiniz",
    ADD_TIME: 'Ek süre ister misiniz?',
    CANCEL: 'İşleminizi iptal etmek istiyor musunuz?',
}

export const MessageCode = {
    TIMEOUT: 'timeout',
    CANCEL: 'cancel',
    WARNING: 'warning',
    EMPTY_DEBTS: 'empty-debts',
    OTHER: 'other',
}

export const MessageDisplayModeEnum = {
    Popup: 0,
    LargePopup: 1,
    Fullscreen: 2,
}

export const MessageActionTypeEnum = {
    None: 0,
    Alert: 1,
    Confirmation: 2,
}

export const MessageResponseEnum = {
    Timeout: 0,
    OK: 1,
    Yes: 2,
    No: 3
}

export function showLoading(message) {
    const dispatch = GlobalKeys.current_dispatch
    dispatch(MessageModuleActions.ShowLoading, message, { root: true })
}

export function showAddTime() {
    const dispatch = GlobalKeys.current_dispatch
    var data = {};
    data.message = {};
    data.message.msg = "Ek süre ister misiniz?"
    data.message.msgType = 2
    data.message.msgCode = MessageCode.TIMEOUT
    data.message.msgActType = MessageActionTypeEnum.Confirmation
    data.message.timeout = 10000

    dispatch(MessageModuleActions.ShowMessage, data, { root: true })
}

export function showCancel() {
    const dispatch = GlobalKeys.current_dispatch
    dispatch(MessageModuleActions.Reset, null, { root: true })

    var data = {};
    data.message = {};
    data.message.msg = MessageContents.CANCEL
    data.message.msgType = 2
    data.message.msgCode = MessageCode.CANCEL
    data.message.msgActType = MessageActionTypeEnum.Confirmation
    data.message.timeout = 120000
    dispatch(MessageModuleActions.ShowMessage, data, { root: true })
}

export function showWarning(message) {
    const dispatch = GlobalKeys.current_dispatch
    dispatch(MessageModuleActions.Reset, null, { root: true })

    var data = {};
    data.message = {};
    data.message.msg = message
    data.message.msgType = 2
    data.message.msgCode = MessageCode.WARNING
    data.message.msgActType = MessageActionTypeEnum.Alert
    data.message.timeout = 15000
    dispatch(MessageModuleActions.ShowMessage, data, { root: true })
}

export function showEmptyDebtList(message) {
    const dispatch = GlobalKeys.current_dispatch
    dispatch(MessageModuleActions.Reset, null, { root: true })

    var data = {};
    data.message = {};
    data.message.msg = message
    data.message.msgType = 2
    data.message.msgCode = MessageCode.EMPTY_DEBTS
    data.message.msgActType = MessageActionTypeEnum.None
    data.message.timeout = 10000
    dispatch(MessageModuleActions.ShowMessage, data, { root: true })
}