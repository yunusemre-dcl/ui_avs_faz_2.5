const moduleName = 'opmakbuzmodule'

export const OpMakbuzMutations = {
    SET_SELECTED_MAKBUZ: 'setSelectedMakbuz',
}

export const OpMakbuzActions = {
    SELECT_MAKBUZ: `${moduleName}/selectMakbuz`,
    SUBMIT: `${moduleName}/submit`,
    PRINT: `${moduleName}/print`,
}