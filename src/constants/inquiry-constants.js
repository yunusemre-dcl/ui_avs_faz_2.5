const moduleName = "debtinquirymodule";

export const InquiryModuleActions = {
    NEXT: `${moduleName}/next`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    CHANGE_VALUE: `${moduleName}/changeValue`,
    SELECTED_QTYPE: `${moduleName}/selectedQtype`,
    SELECTED_VALUE_CLEAR: `${moduleName}/clearValue`,
    RESET_VALUE: `${moduleName}/resetValue`,
}

export const InquiryModuleMutations = {
    SET_QUERY_TYPES: 'setQueryTypes',
    SET_SELECTED_QTYPE: 'setSelectedQtype',
    SET_VALUE: 'setValue',
    SET_VALUE_CLEAR: 'setValueClear',
    RESET: 'reset',
    RESET_VALUE: 'resetValue',
}