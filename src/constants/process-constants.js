const moduleName = "processmodule";

export const ProcessModuleActions = {
    SELECT: `${moduleName}/select`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
};

export const ProcessModuleMutations = {
    SET_SVCS : 'setSvcs',
}

export const ProcessCompanyGroups = {
    EKS: 1,
}