const moduleName = "debtselectionmodule";

export const DebtSelectionActions = {
    NEXT: `${moduleName}/next`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    SELECTED: `${moduleName}/itemSelected`,
    ALL_SELECTED: `${moduleName}/allItemSelected`,
}

export const DebtSelectionMutations = {
    SET_DEBTS: 'setDebts',
    SET_SELECTED_BILL: 'setSelectedBill',
    CHANGE_SELECTED_ALL: 'changeSelectedAll',
    RESET: 'reset',
}