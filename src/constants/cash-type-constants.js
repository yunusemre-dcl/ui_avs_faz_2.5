const moduleName = "cashtypemodule";

export const CashTypeActions = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    CASH_TYPE_SELECTED: `${moduleName}/cashTypeSelected`,
}

export const CashTypeMutations = {
    SET_PAY_TYPES : 'setPayTypes',
}