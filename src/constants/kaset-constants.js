const moduleName = "kasetmodule";

export const KasetMutations = {
    SET_KASET_LIST: 'setkasetList',
    SET_SELECTED_UNITE: 'setSelectedUnit',
    UPDATE_KASET: 'updatekaset',
}

export const KasetActions = {
    SELECT_UNITE: `${moduleName}/selectUnite`,
    UPDATE_KASET: `${moduleName}/updateKaset`,
    SAVE: `${moduleName}/save`,
}

export default {
    KasetActions,
    KasetMutations
}