const moduleName = 'eodmodule'

export const EodActions = {
    PRINT: `${moduleName}/print`,
    PROCESS: `${moduleName}/process`,
    MENU: `${moduleName}/menu`,
}

export const EodMutations = {
    SET_LOAD_DATA: 'setLoadData',
}