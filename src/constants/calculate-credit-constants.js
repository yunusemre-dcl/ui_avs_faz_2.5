const moduleName = "calculatecreditmodule";

export const CalculateCreditActions = {
    NEXT: `${moduleName}/next`,
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
    CHANGE_AMOUNT: `${moduleName}/changeAmount`,
}

export const CalculateCreditMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    CHANGE_AMOUNT: 'changeAmount',
}