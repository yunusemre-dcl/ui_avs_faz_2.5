const moduleName = `oprecyclermodule`

export const CashDispenseMutations = {
    SET_LOAD_DATA: 'setLoadData',
    ADD_LOG: 'addLog',
    UPDATE_BANKNOTES: 'updateBanknotes',
    UPDATE_RESET_BUTTON_STATE: 'updateResetBtnState',
    UPDATE_MENU_BUTTON_STATE: 'updateMenuButtonState',
    UPDATE_END_BUTTON_STATE: 'updateEndButtonState',
}

export const CashDispenseActions = {
    BUTTON_STATE: `${moduleName}/buttonState`,
}

export const BtnTypes = {
    START: 1,
    STOP: 2,
    RESET: 3,
    NEXT: 4,
    BACK: 5,
    MENU: 6,
}