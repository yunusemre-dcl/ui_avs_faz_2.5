const moduleName = "creditcardmodule";

export const CreditCardActions = {
    BACK: `${moduleName}/back`,
    CANCEL: `${moduleName}/cancel`,
}

export const CreditCardMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    CLICKED_CANCEL: 'clickedCancel',
}