export const MessageDisplayModeEnum = {
    Popup: 0,
    LargePopup: 1,
    Fullscreen: 2,
}

export const MessageActionTypeEnum = {
    None: 0,
    Alert: 1,
    Confirmation: 2,
}

export const MessageResponseEnum = {
    Timeout: 0,
    OK: 1,
    Yes: 2,
    No: 3
}