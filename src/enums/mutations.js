

export const InitializeMutations = {
    SET_MESSAGES : "setMessages",
}

export const IntroMutations = {
    SET_VIDEO : 'setVideo',
    SET_SLIDER : 'setSlider',
    SET_TYPE : 'setType'
}

export const LangManagementMutations = {
    SET_DICTIONARY : "setDictionary",
    GET_VALUE : 'getValue',
    SET_VALUE : 'setValue',
}

export const LangMutations = {
    SET_LANGUAGES : 'setLanguages',
    SET_BUTTON_LABELS : 'setButtonLabels',
    SET_LANG_DICTIONARY : 'setLangDictionary',
}


export const DebtInquiryMutations = {
    SET_QUERY_TYPES: 'setQueryTypes',
    SET_SELECTED_QTYPE: 'setSelectedQtype',
    SET_VALUE_CLEAR: 'setValueClear',
}

export const CardReaderMutations = {
    SET_DATA: 'setData',
    CHANGE_DATA: 'changeData',
    SET_TOTAL_BILL: 'setTotalBill',
    SET_CUSTOMER: 'setCustomer',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
    CLICKED_CANCEL: 'clickedCancel',
}

export const PaybackMutations = {
    SET_MESSAGE: "setMessage",
    SET_TIMEOUT: "setTimeout",
    REFRESH: "refresh",
}

export const PrintMutations = {
    SET_DATA : 'setData',
    SET_LANG_DICTIONARY: 'setLangDictionary',
    SET_BUTTON_LABELS: 'setButtonLabels',
}