export const ScreenEnum = {
    //Initialize: 1,
    Init: 2,
    Unit: 3,
    DebtInquiry: 4,
    DebtSelection: 5,
    CashType: 6,
    CashAccepting: 7,
    Payback: 8,
    Print: 9,
    CreditCard: 10,
    CardReader: 11,
    CalculateCredit: 12,
    EksLoad: 13,


    // Operator Screens

    OperatorLogin: 14,
    OperatorMenu:15,
    OperatorCashDispense: 16, // Recycler
    OperatorCoin: 17, // Silo
    OperatorKaset: 18,
    // OperatorReport: 19, // EOD
    // OperatorMakbuz: 17,
    // OperatorAlarm: 20,
}