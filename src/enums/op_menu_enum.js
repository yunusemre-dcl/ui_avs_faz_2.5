export const OpMenuEnum = {
    Test: 1,
    Receipt: 2,
    Report: 3,
    Set: 4,
    Alarms: 5,
    Maintenance: 6,
    Reset: 7,
    Exit: 8,
    PowerOff: 9
}