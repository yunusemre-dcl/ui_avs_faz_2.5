export const AnimationCodeEnum = {
    None: 0,
    InsertYourCard: 1,
    EnterYourPassword: 2,
    GetYourCard: 3,
    ReadCard: 4,
}